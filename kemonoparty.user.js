// ==UserScript==
// @name         Kemonoparty fixes
// @namespace    https://gitlab.com/krearin/
// @version      0.30
// @description  Kemonoparty improvements and fixes.
// @author       Krearin
// @match        https://kemono.party/*/user/*
// @match        https://kemono.su/*/user/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=kemono.su
// @require      https://cdnjs.cloudflare.com/ajax/libs/jszip/3.9.1/jszip.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/FileSaver.js/2.0.0/FileSaver.min.js
// @require      https://gitlab.com/krearin/userscripts/-/raw/main/helpers/TouchEvent.js
// @grant        GM.xmlHttpRequest
// @grant        GM.getValue
// @grant        GM.setValue
// @connect      kemono.party
// @connect      kemono.su
// @run-at       document-start
// ==/UserScript==

/* Supported:
 * - All Features: Patreon and Fanbox
 * - Some Features: Fantia (no link fix)
 *
 * Features:
 * - Link-fixes: View them on kemono | Kemono implemented this by itself but not everywhere
 * - Sort DMs by Date
 * - Download button for images and videos
 * - Translation feature (libretranslate)
 * - Prevent loading of images from other hosts (fanbox 403 spam)
 */

// Issues:
// Post Downloadbutton may not work correctly

// TODO: Save last used translation settings
// TODO: code cleanup - urgently lol
// TODO: Research if "live updating" config is possible? (mark live updates with a star? - DMs sorting should be possible without reload)
// TODO: Improve styling of config window - surround config groups with border?

const kemono_domains = [
    "kemono.party",
    "kemono.su"
]

const url_info = window.location.href.match(/https?:\/\/w{0,3}\.?(?<domain>[\w\.]+)\/(?<platform>\w+)\/user\/(?<uid>\d+)\/?(?<ptype>\w+)?\/?(?<tid>\d+)?\/?/).groups
const page_type = url_info.ptype || "unknown"
// https://stackoverflow.com/a/11381730
const isMobile = (a=>{return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))})(navigator.userAgent||navigator.vendor||window.opera);

// Prevent loading (forbidden) images from other hosters (fanbox in particular)
const whitelist_pattern = /^(?<protocol>(?:https?:)?\/\/)?(?<domain>(?:[\w\-]+\.)?(?:kemono\.party|kemono\.su|imgur\.com|googleusercontent\.com))\/(?<filename>[^\.]+)(?:\.(?<extension>png|gif|webp|jpe?g|tiff?|bmp|hei(?:f|c)|svg))?/

const allowedImages = []
const removedImages = []
const removeImagesrc = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAABAAAAAQACAYAAAB/HSuDAAAABmJLR0QA/wAAANy8QuIMAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH4QwOEws1n8T4dAAAAB1pVFh0Q29tbWVudAAAAAAAQ3JlYXRlZCB3aXRoIEdJTVBkLmUHAAAWmUlEQVR42u3aQQ1EMQxDQWf1oZQ/onLJ4qg8AyGSe3jqbO6GWpPjCMU21xHsH/vH/rF/7J8SPycAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAABAAAAAAADeNknWGXptriNUPwDHEewf+8f+sX/snxJ+AAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAKAEwAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAACAAAAAAAA87ttcVyg2OY5QzP7tH/vH/rF/7J8efgAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAACgBMAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgAAAAAgAAAAAAAPG6SrDP02lxHqH4AjiPYP/aP/WP/2D8l/AAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEACcAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAAAAAEAAAAAeNy3ua5QbHIcoZj92z/2j/1j/9g/PfwAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAnAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAAAQAAAAAAABAAAAAHjcJFln6LW5jlD9ABxHsH/sH/vH/rF/SvgBAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACABOAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgAAAAAAACAAAAAPC4b3NdodjkOEIx+7d/7B/7x/6xf3r4AQAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgATgAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAgAAAAAIAAAAAAAAgAAAAAgAAAAAAACAAAAACAAAAAAAAIAAAAAIAAAAAAAAgAAAAAIAAAAAAAAgAAAADwuEmyztBrcx2h+gE4jmD/2D/2j/1j/5TwAwAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAnAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAAEAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAAABAAAAABAAAAAAAAEAAAAAEAAAAAAAAQAAAAAQAAAAAAABAAAAABAAAAAAQAAAAAAABAAAAADgcX9hnkxpPzJJmQAAAABJRU5ErkJggg=="
const checkImg = (el) => {
    const src = el.getAttribute("src")
    if(src != removeImagesrc && !src.match(/^\/{1}[^/]/)) {
        const info = src.match(whitelist_pattern)?.groups
        if(info == null) {
            removedImages.push(src)
            el.setAttribute("src", removeImagesrc)
        } else {
            allowedImages.push(src)
        }
    }
}

const checkNestedNodes = (node, fun) => {
    if(node instanceof HTMLElement) {
        if(node.tagName.toLowerCase() === "img" &&
          node.getAttribute("src") !== "") {
            fun(node)
        }
        for(const child of node.childNodes) {
            checkNestedNodes(child, fun)
        }
    }
}

const observer = new MutationObserver(mutations => {
    for(const mutation of mutations) {
        if(mutation.type == "childList") {
            for (const node of mutation.addedNodes) {
                if(node instanceof HTMLElement) {
                    checkNestedNodes(node, checkImg)
                }
            }
        }
        if(mutation.attributeName === "src" &&
          (mutation.target instanceof Element) &&
          mutation.target.tagName.toLowerCase() === "img" &&
          mutation.target.getAttribute("src") !== "" ) {
            checkImg(mutation.target)
        }
    }
})
observer.observe(document, {
    childList : true,
    subtree : true,
    attributes : true,
    attributeOldValue : true,
    characterDataOldValue : true
})

const config_fields = [
    {
        "id": "fix_urls",
        "label": "Fix Urls which are not fixed already by kemono",
        "type": "checkbox",
        "default": true,
        "isfeature": true,
        "allow": ["post","announcements"],
    },
    {
        "id": "fix_dms",
        "label": "Fix DMs (sorting)",
        "type": "checkbox",
        "default": true,
        "isfeature": true,
        "allow": ["dms"],
    },
    {
        "id": "sort_dms",
        "label": "DMs sorting",
        "type": "choicebox",
        "values": [ "Oldest first", "Newest first" ],
        "default": 0,
        "requires": "fix_dms",
    },
    {
        "id": "download_btn",
        "label": "Add download button for Videos and Files",
        "type": "checkbox",
        "default": true,
        "isfeature": true,
        "allow": ["post"],
    },
    {
        "id": "download_hq",
        "label": "High quality images (downloads currently loaded images if disabled)",
        "type": "checkbox",
        "default": false,
        "requires": "download_btn",
    },
    {
        "id": "translation",
        "label": "Add offline LibreTranslate",
        "type": "checkbox",
        "default": false,
        "isfeature": true,
    },
    {
        "id": "auto_translation",
        "label": "Automaticaly Translate",
        "type": "checkbox",
        "default": false,
        "requires": "translation",
    },
    {
        "id": "translationurl",
        "label": "LibreTranslate url (local instance recommended)",
        "type": "text",
        "default": "http://localhost:5000/translate",
        "requires": "translation",
    },
    {
        "id": "navigation_help",
        "label": "Add (mobile) navigational help",
        "type": "checkbox",
        "default": true,
        "isfeature": true,
        "mobileonly": true,
        "allow": ["post"],
    },
]

const loaded_config = {}

const continueMain = (mainbar, features, log) => {

    log("debug", "urlinfos: " + Object.keys(url_info).map(info => `${info}: ${url_info[info]}`).join(", "))
    log("debug", "imgwhitelist: " + whitelist_pattern.toString())

    // log removed images
    removedImages.forEach(img => log("Forbidden Images", img))
    allowedImages.forEach(img => log("Allowed Images", img))

    const configTab = document.createElement("div")
    configTab.setAttribute("id", "kpfix_config_main")
    configTab.classList.add("kpfix_main")
    mainbar.append(configTab)

    const loadConfig = async () => {
      return await Promise.all(config_fields.map(async field => {field.value = await GM.getValue(field.id, field.default); return field}))
    }

    loadConfig().then(config => {
        const config_elements = []
        Object.keys(loaded_config).forEach(key => delete object[key])
        const loading_features = []
        config.forEach(field => {
            const setHidden = field.mobileonly && !isMobile
            //const setDisabled = field.mobileonly && !isMobile

            if(setHidden) {
                return
            }

            let el = createFormElement(configTab, "kpfix_config_field_"+field.id, field.label, field.value, field.type, true, field.id, field.values)
            config_elements.push(el)

            // Hide config element if it is not allowed for this page (ex.: features)
            // or if its required (feature) is not activated or not allowed on this page
            // TODO: change this if statement to something readable....
            if(
                ("allow" in field && !field.allow.includes(page_type)) ||
                (!field.isfeature && "requires" in field &&
                    (!loaded_config[field.requires].value ||
                    (("allow" in loaded_config[field.requires]) && !loaded_config[field.requires].allow.includes(page_type)))
                )
            ) {
                el.parentElement.parentElement.removeChild(el.parentElement)
            }

            // load feature if it is activated and the page is correct
            if(field.value === true && field.isfeature && (!("allow" in field) || field.allow.includes(page_type))) {
              loading_features.push(field.id)
            }

            loaded_config[field.id] = field
        })

        const config_save_btn = document.createElement("button")
        config_save_btn.setAttribute("id", "kpfix_cfg_save")
        config_save_btn.innerText = "Save"
        config_save_btn.onclick = e => {
            log("config", "saving...")
            config_elements.forEach(el => {
                let value = el.type == "checkbox" ? el.checked : el.value
                let id = el.dataset.fieldid

                if(id == null || typeof id === "undefined") return

                GM.setValue(id, value).catch(err => {
                  console.error(err)
                  log("config", err)
                })
            })

          const reload_btn = document.createElement("button")
          reload_btn.setAttribute("id", "kpfix_cfg_reloadpage")
          reload_btn.innerText = "Reload page"
          reload_btn.onclick = e => location.reload()
          configTab.append(reload_btn)
        }
        configTab.append(config_save_btn)
        loading_features.forEach(feature => features[feature]())
    })
}

window.addEventListener("load", (async function() {
    'use strict';
    //observer.disconnect()

    const features = {
        "fix_urls": () => {
            const patterns = {
                "patreon": { inc: "patreon.com/posts/", link: /^https?:\/\/w{0,3}\.?patreon\.com\/posts\/(?:[\w\-\d]*\-|[\w\-\d]*?)(\d+)\/?$/ },
                "fanbox": { inc: "fanbox.cc/@", link: /^https?:\/\/w{0,3}\.?fanbox\.cc\/@.*?\/posts\/(?:[\w-]*?)(\d+)\/?$/ }
            }
            const supported_plattform = Object.keys(patterns).includes(url_info.platform)
            if(supported_plattform) {
                const pattern = patterns[url_info.platform]
                // Fix links and emphasize them with a border
                ;[...document.getElementsByTagName("a")].filter(el => el.href.includes(pattern.inc)).forEach(
                    el => {
                        const oh = el.href
                        el.href = "https://" + url_info.domain + "/" + url_info.platform + "/post/" + (oh.match(pattern.link) ?? [,oh])[1]
                        if(oh != el.href) {
                            el.style.border="1px solid white"
                        }
                    })
            } else {
              log("Linkfix", "Unsupported platform: " + url_info.platform)
            }
        },
        "download_btn": () => {
            // warn download button error (not fixed yet)
            log("Issues", "Currently the download button may not be functional", true)

            addGlobalStyle('.kpfix_showdl { border: 10px dashed purple; }')
            addGlobalStyle('div.fluid_video_wrapper { margin-bottom: 1.5em; }') // firefox supports :has now but I forgot what I wanted to do ...

            const downloadTab = document.createElement("div")
            downloadTab.setAttribute("id", "kpfix_download_main")
            downloadTab.classList.add("kpfix_main")
            mainbar.append(downloadTab)

            const hq = loaded_config.download_hq.value
            let post_files = [...document.getElementsByClassName("post__thumbnail")].map(
                (thumb, index) => {
                    return {
                        url: hq ? thumb.children[0].getElementsByTagName("a")[0].href : thumb.children[0].getElementsByTagName("img")[0].src, name:decodeURI(index + "_" + thumb.children[0].getElementsByTagName("a")[0].download), el:thumb
                    }
                }
            )

            if(post_files.length > 0)
            {
                post_files[0].name = "_thumbnail." + post_files[0].name.split(".").slice(-1)
                post_files[0].el.setAttribute("id", "kpfix_thumbnail")
            }

            let post_videos = [...document.getElementsByClassName("post__video")].map(
               video => ({url:video.currentSrc,name:video.closest("li").getElementsByTagName("summary")[0].innerText,el:video}))

            let files = [
                ...post_files,
                ...post_videos
            ]

            let total = files.length

            var download_info = document.createElement("span")
            downloadTab.append(download_info)

            const include_thumbnail = createFormElement(downloadTab, "kpfix_chk_thumbnail", "include Thumbnail", false, "checkbox", true)
            const show_download_objects = createFormElement(downloadTab, "kpfix_chk_dl_objects", "Highlight downloadable contents (border)", true, "checkbox", true)

            var dlbtn = document.createElement("button")
            dlbtn.setAttribute("id", "dlbtn")
            dlbtn.innerText = "Download"
            downloadTab.append(dlbtn)

            let toggleDlBorder = () => {
              let show = show_download_objects.checked
              let thumbshow = include_thumbnail.checked
              download_info.innerText = (total - (!thumbshow && post_files.length > 1)) + " Downloads from 'files' and/or 'videos' available"
              files.forEach(file => {
                let isthumb = file.el.id == "kpfix_thumbnail"
                if((isthumb && thumbshow) || (!isthumb && show)) {
                  file.el.classList.add("kpfix_showdl")
                } else {
                  file.el.classList.remove("kpfix_showdl")
                }
              })
            }
            include_thumbnail.addEventListener("change", toggleDlBorder)
            show_download_objects.addEventListener("change", toggleDlBorder)

            if(post_files.length == 0) {
              include_thumbnail.setAttribute("disabled", true)
            }

            var zip = new JSZip()

            let dl = url => {
                return new Promise((resolve, reject) => {
                    GM.xmlHttpRequest({
                        method: "GET",
                        url: url,
                        responseType: "blob",
                        onload: response => {
                        if(response.status == 200) {
                            let reader = new FileReader();
                            reader.onloadend = function() {
                                let uri = reader.result
                                let idx = uri.indexOf('base64,') + 'base64,'.length
                                resolve(uri.substring(idx))
                            }
                            reader.readAsDataURL(response.response);
                        } else {
                            reject({
                                status: response.status,
                                statusText: response.statusText,
                                error: response.error
                            })
                        }},
                        onerror: response => {
                        reject({
                            status: response.status,
                            statusText: response.statusText,
                            error: response.error
                        })}
                    })
                })
            }

            if(!include_thumbnail.disabled && !include_thumbnail.checked) {
              files.shift()
              total--
            }

            let progressbar = document.createElement("progress")
            progressbar.setAttribute("max", total)
            progressbar.value = 0
            downloadTab.append(progressbar)

            let gen_zip = () => {
                progressbar.value = total-files.length
                if(files.length > 0) {
                    let file = files.shift()
                    if(file.name == "undefined") {
                        console.error("Failed to get filename", file)
                        return
                    }
                    log("gen_zip", "File " + (total-files.length) + "/" + total + ": " + file.name)
                    dl(file.url).then(data => zip.file(file.name, data, {base64:true})).then(gen_zip).catch(err => {
                        console.error(err);
                        log("gen_zip", err)
                        if(err.status == 0 && err.statusText == "") {
                            console.error("genzip failed with no Error message (CORS error?)")
                            log("gen_zip", "genzip failed with no Error message (CORS error?)")
                        }
                    })
                } else {
                    log("gen_zip", "Loaded " + Object.keys(zip.files).length + " files.")
                    zip.generateAsync({type:"blob"}).then(
                        data => saveAs(data, document.getElementsByClassName("post__title")[0].innerText + "_files.zip")
                    ).catch(err => {console.error(err); log("zip.generateAsync error", err)})
                }
            }

            if(total == 0) {
              include_thumbnail.setAttribute("disabled", true)
              show_download_objects.setAttribute("disabled", true)
            } else {
                dlbtn.onclick = e => {
                    dlbtn.setAttribute("disabled", "true")
                    gen_zip()
                }
            }

            toggleDlBorder()

      },
        "translation": () => {
          const translate = (from, text) => {
              return new Promise((resolve, reject) => {
                  GM.xmlHttpRequest({
                      method: "POST",
                      url: loaded_config.translationurl.value,
                      data: JSON.stringify({
                        q: text,
                        source: from,
                        target: "en",
                        format: "text"
                      }),
                      headers: {
                          "Content-Type": "application/json"
                      },
                      onload: response => {
                      if(response.status == 200) {
                          resolve(response.response)
                      } else {
                          reject({
                              status: response.status,
                              statusText: response.statusText,
                              error: response.error
                          })
                      }},
                      onerror: response => {
                      reject({
                          status: response.status,
                          statusText: response.statusText,
                          error: response.error
                      })}
                  })
              })
          }

          addGlobalStyle('.kpfix_translated { text-decoration-line: underline; text-decoration-style: dotted; text-decoration-color: purple; text-decoration-thickness: 0.25rem; }')

          const translationTab = document.createElement("div")
          translationTab.setAttribute("id", "kpfix_translate_main")
          translationTab.classList.add("kpfix_main")
          mainbar.append(translationTab)

          const inputs = {}

          inputs.langsel_input = createFormElement(translationTab, "kpfix_translate_langfromsel", "Translate from: ", "auto", "text", true)

          if(page_type == "post") {
            inputs.translate_title = createFormElement(translationTab, "kpfix_translate_title", "Translate page title", true, "checkbox", true)
            inputs.translate_content = createFormElement(translationTab, "kpfix_translate_content", "Translate content", true, "checkbox", true)
            inputs.translate_download_titles = createFormElement(translationTab, "kpfix_translate_download_titles", "Translate downloadlink texts", true, "checkbox", true)
            inputs.translate_download_filenames = createFormElement(translationTab, "kpfix_translate_download_filenames", "Translate downloaded file names", true, "checkbox", true)
            inputs.translate_video_files = createFormElement(translationTab, "kpfix_translate_video_files", "Translate video filenames", true, "checkbox", true)
            inputs.translate_comments = createFormElement(translationTab, "kpfix_translate_comments", "Translate comments", false, "checkbox", true)

            // not working yet
            inputs.translate_download_filenames.checked = false
            inputs.translate_download_filenames.disabled = true
            inputs.translate_comments.checked = false
            inputs.translate_comments.disabled = true
          }

          const translate_btn = createFormElement(translationTab, "kpfix_translate_btn", "Translate", true, "button", false)

          const translate_post = e => {
            const langsel = inputs.langsel_input.value ?? "auto"
            const t_title = inputs.translate_title.value
            const t_content = inputs.translate_content.value
            const t_download_titles = inputs.translate_download_titles.value
            const t_download_names = inputs.translate_download_filenames.value
            const t_video_files = inputs.translate_video_files.value

            const translate_elements_normal = []
            const translate_elements_links = []

            const post__title = document.getElementsByClassName("post__title")[0]
            const post__content = document.getElementsByClassName("post__content")[0]
            const post__attachment = document.getElementsByClassName("post__attachment-link")

            if(t_title && typeof post__title !== "undefined") {
              translate_elements_normal.push(...post__title.children)
            }

            if(t_content && typeof post__content !== "undefined") {
              translate_elements_normal.push(...post__content.children)
            }

            if(t_download_titles && typeof post__attachment !== "undefined") {
              translate_elements_links.push(...post__attachment)
            }

            if(t_download_names) { // dunno if possible

            }

            if(inputs.translate_video_files) {
              translate_elements_links.push(...document.getElementsByTagName("summary"))
            }

            // dunno if this works for everywhere
            translate_elements_normal.forEach(el => {
              const txt = el.innerHTML
              if(txt == "") return
              translate(langsel, el.innerHTML).then(traw => {
                  const trans = JSON.parse(traw)
                  if((langsel != "auto" || trans.detectedLanguage.language != "en") && el.innerHTML != trans.translatedText) {
                      el.innerHTML = trans.translatedText
                      el.classList.add("kpfix_translated")
                  }
                  el.dataset.detectedLanguage = trans.detectedLanguage.language ?? langsel
                }).catch(err => { console.error(err); log("translate normal error", err) })
            })

            translate_elements_links.forEach(el => {
              const isdl = el.innerText.startsWith("Download ")
              if(isdl) {
                el.innerText = el.innerText.split("Download ")[1]
              }
              const org_file = el.innerText.replace(/\.[^/.]+$/, "")
              const extension = el.innerText.match(/(\.[^/.]+)$/)[1]
              translate(langsel, org_file).then(traw => {
                  const trans = JSON.parse(traw)
                  el.innerText = (isdl?"Download " : "") + trans.translatedText + "(" + org_file + ")" + extension
                  el.dataset.detectedLanguage = trans.detectedLanguage.language ?? langsel
                }).catch(err => { console.error(err); log("translate links error", err) })
            })
          }
          const translate_overview = e => {
            const langsel = inputs.langsel_input.value ?? "auto"

            const translate_elements = [...document.getElementsByClassName("post-card__header")]

            // dunno if this works for everywhere
            translate_elements.forEach(el => {
              translate(langsel, el.innerHTML).then(traw => {
                  const trans = JSON.parse(traw)
                  if((langsel != "auto" || trans.detectedLanguage.language != "en") && el.innerHTML != trans.translatedText) {
                      el.innerHTML = trans.translatedText
                      el.classList.add("kpfix_translated")
                  }
                  el.dataset.detectedLanguage = trans.detectedLanguage.language ?? langsel
                }).catch(err => { console.error(err); log("translate overview error", err) })
            })
          }
          if(loaded_config.auto_translation.value) {
            translate_btn.disabled = true
            if(page_type == "post") {
                translate_post()
            } else {
                translate_overview()
            }
          } else {
            translate_btn.onclick = page_type == "post" ? translate_post : translate_overview
          }
        },
        "fix_dms": () => {
            const latest_first = true
            const old_container = document.getElementsByClassName("card-list__items")[0]

            const new_container = document.createElement("div")
            new_container.classList.add("card-list__items")
            new_container.setAttribute("id", "kpfix_dms")
            old_container.parentElement.append(new_container)
            old_container.parentElement.removeChild(old_container)

            ;[...old_container.getElementsByClassName("dm-card")].map((card, i) => {
                const date_raw = card.getElementsByClassName("dm-card__added")[0].innerText
                const date = new Date(date_raw.substring(date_raw.indexOf(":") + 2) + "-01")
                return {
                    html: card,
                    date: date.getTime(),
                    i: i
                }
            }).sort((a, b) =>
              loaded_config.sort_dms.value == 0 ? (a.date - b.date) + (a.i - b.i)
                : (b.date - a.date) + (a.i - b.i)
            ).forEach(dm => {
                new_container.append(dm.html)
            })
        },
        "navigation_help": () => {
            const prev_link = document.getElementsByClassName("post__nav-link prev")[0]
            const next_link = document.getElementsByClassName("post__nav-link next")[0]

            if(prev_link || next_link) {
                let touchEvent = null
                const handleSwipe = event => {
                    if(!touchEvent) {
                        return
                    }

                    touchEvent.setEndEvent(event)

                    if(touchEvent.isSwipeRight() && prev_link) {
                        prev_link.click()
                    } else if(touchEvent.isSwipeLeft() && next_link) {
                        next_link.click()
                    }

                    touchEvent = null
                }

                document.addEventListener("touchstart", event => {
                    touchEvent = new TouchEvent(event)
                })

                document.addEventListener("touchend", handleSwipe)

            }
        }
    }

    addGlobalStyle('.kpfix_main { border:.125em solid var(--colour0-tertirary); border-radius: 10px; background-color: var(--colour1-secondary); padding: 1.5em 2em; margin: 1em auto; }')
    addGlobalStyle('.kpfix_main > :not(.kpfix_main, label, input) { margin: 0 1em; }')
    addGlobalStyle('.kpfix_main > :is(label, input) { margin: 0 0.2em; }')
    addGlobalStyle('.kpfix_main > input[type=checkbox]:disabled+label { color: #ccc; }')
    addGlobalStyle('.kpfix_main > .row { display:block; }')
    addGlobalStyle('.kpfix_main > .row.important { color:#ea0; font-weight:bold; }')
    addGlobalStyle('.kpfix_main > .showlist > .important { color:#ea0; }')
    addGlobalStyle('.kpfix_main > ul.hiddenlist > li { display:none; }')
    addGlobalStyle('.kpfix_main > ul.showlist > li { display:block; }')
    addGlobalStyle('.kpfix_main:empty { display:none; }')

    // container for all kpfix tabs
    const mainbar = document.createElement("div")
    mainbar.setAttribute("id", "kpfix_main")
    mainbar.classList.add("kpfix_main")
    mainbar.innerHTML = ""

    // logging
    const logTab = document.createElement("div")
    logTab.setAttribute("id", "kpfix_log_main")
    logTab.classList.add("kpfix_main")
    mainbar.append(logTab)

    const logged_messages = {}
    const loggedstateswitch = {
        "hiddenlist": "showlist",
        "showlist": "hiddenlist"
    }
    const loggedstateswitchicon = {
        "hiddenlist": "+",
        "showlist": "-"
    }
    const logclick = (src, container) => {
        container.classList.remove(container.dataset.state)
        container.dataset.state = loggedstateswitch[container.dataset.state]
        container.dataset.stateicon = loggedstateswitchicon[container.dataset.state]
        container.classList.add(container.dataset.state)

        document.getElementById(src.replace(/\W/g, '').toLowerCase() + "head").innerText = `[${container.dataset.stateicon}] ${logged_messages[src].count} x ${src}`
    }
    const log = (src, msg, important = false) => {
        if(typeof msg === "object") {
            msg = JSON.stringify(msg)
        }
        if(logged_messages[src] == null) {
            const new_log_container = document.createElement("ul")
            new_log_container.dataset.state = "hiddenlist"
            new_log_container.dataset.stateicon = loggedstateswitchicon[new_log_container.dataset.state]
            new_log_container.classList.add(new_log_container.dataset.state)

            logged_messages[src] = {
                el: new_log_container,
                count: 0
            }

            const head = document.createElement("span")
            head.id = src.replace(/\W/g, '').toLowerCase() + "head"
            head.innerText = `[${new_log_container.dataset.stateicon}] ? x ${src}`
            head.classList.add("row")
            if(important) {
                head.classList.add("important")
            }
            head.addEventListener("click", () => { logclick(src, new_log_container) })

            logTab.append(head, new_log_container)
        }

        const old_log_container = logged_messages[src]
        old_log_container.count++

        const new_log = document.createElement("li")
        new_log.innerText = msg
        if(important) {
            new_log.classList.add("important")
            document.getElementById(src.replace(/\W/g, '').toLowerCase() + "head").classList.add("important")
        }

        setTimeout(() => {
            old_log_container.el.append(new_log)

            if(important && old_log_container.el.dataset.state == "hiddenlist") {
                logclick(src, old_log_container.el)
            }
        }, old_log_container.count + 100) // :(

        document.getElementById(src.replace(/\W/g, '').toLowerCase() + "head").innerText = `[${old_log_container.el.dataset.stateicon}] ${old_log_container.count} x ${src}`
    }

    const mainbarobserver = new MutationObserver((mutations, observer) => {
        const req_el_name = page_type == "post" ? "post__body" : "user-header"
        const el = document.getElementsByClassName(req_el_name)[0]

        if (el) {
            // No need to observe anymore. Clean up!
            observer.disconnect();

            if(page_type == "post") {
                el.prepend(mainbar)
            } else {
                el.parentNode.insertBefore(mainbar, el.nextSibling)
            }

            continueMain(mainbar, features, log)
        }
    });
    mainbarobserver.observe(document, {
        childList : true,
        subtree : true
    })
}))


const addGlobalStyle = (css) => {
    const head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    const style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

const createFormElement = (parent, id, text, value = true, type = "checkbox", label = false, fieldid = false, values = []) => {
    const el_type = type == "button" ? "button"
                  : type == "choicebox" ? "select"
                  : "input"
    const el = document.createElement(el_type)
    const row = document.createElement("div")
    row.classList.add("row")
    parent.append(row)

    if(typeof fieldid == "string" && fieldid !== null && fieldid.trim() != "") {
        el.dataset.fieldid = fieldid
    }

    el.setAttribute("id", id)

    if(type == "choicebox") {
        if(values.length > 0) {
            values.forEach((val, i) => {
                const option = document.createElement("option")
                option.setAttribute("value", i)
                if(value != null && i === value) {
                    option.setAttribute("selected", "selected")
                }
                option.innerText = val
                el.append(option)
            })
        }
        row.append(el)
    }

    if(type != "choicebox" && type != "button") {
        el.setAttribute("type", type)
    }

    if(type == "checkbox") {
        el.checked = value
    } else {
        el.value = value
    }

    if(type == "checkbox") {
        row.append(el)
    }

    if(type != "button" && typeof label !== "undefined" && typeof label !== null && label !== false) {
        const label = document.createElement("label")
        label.setAttribute("for", id)
        label.innerText = text
        row.append(label)
    } else {
        el.innerText = text
    }

    if(type != "checkbox") {
        row.append(el)
    }

    return el
}
