# userscripts

## Installation

Use 
1. Install [Tampermonkey](https://www.tampermonkey.net/), [Violentmonkey](https://github.com/violentmonkey/violentmonkey) or other compatible addons.
1. Select a script you wish to use. View the file and click the _open Raw_ button at the top of the file to view its source
1. Tampermonkey should detect it and allow you to import it
1. If it doesn't:
   1. Copy the source
   1. Open Tampermonkey in your browser and click the Add Script tab (icon with a plus symbol)
   1. Paste the source into the script window and hit save