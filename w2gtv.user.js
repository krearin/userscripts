// ==UserScript==
// @name        playlist hider
// @namespace   Violentmonkey Scripts
// @match       https://w2g.tv/en/room/*
// @grant       none
// @version     1.0
// @description Better video player.
// @author      Krearin
// @require https://cdn.jsdelivr.net/npm/@violentmonkey/dom@2
// ==/UserScript==

/*
Adds a Button on the top right to show/hide the playlist and left sidebar
Hides by default
*/

let plvisnode = null
let plvisibility = false
let plnode = null
let sbnode = null

const toggle_visibility = () => {
    if(plvisnode && plnode && sbnode) {
        plvisibility = !plvisibility
        plnode.style.display = plvisibility ? "" : "none"
        sbnode.style.display = plvisibility ? "" : "none"
        plvisnode.textContent = plvisibility ? "Hide PL" : "Show PL"
    }
}

const disconnect_pl = VM.observe(document.body, () => {
    // Find the target node
    const node = document.querySelector('#w2g-right');

    if (node) {
        plnode = node
        node.style.display = plvisibility ? "" : "none"
        return true;
    }
});
const disconnect_sidebar = VM.observe(document.body, () => {
    // Find the target node
    const node = document.querySelector('div.h-dvh.shrink-0.sticky.top-0');

    if (node) {
        sbnode = node
        node.style.display = plvisibility ? "" : "none"
        return true;
    }
});

const disconnect_menu = VM.observe(document.body, () => {
    // Find the target node
    const node = document.querySelector('#menu-button');

    if (node) {
        const a = document.createElement('a');
        plvisnode = a
        a.textContent = plvisibility ? "Hide PL" : "Show PL";
        a.classList.add("ml-4")
        a.style.cursor = "pointer"
        a.addEventListener("click", () => {
            toggle_visibility()
        })

        const rightelement = node.parentNode.parentNode.parentNode
        rightelement.parentNode.insertBefore(a, rightelement)
        return true;
    }
});
const disconnect_video = VM.observe(document.body, () => {
    // Find the target node
    const node = document.querySelector('.w2g-player-width');

    if (node) {
        node.style.maxWidth = ""
        return true;
    }
});
